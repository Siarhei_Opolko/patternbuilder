﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Builder.Builders;

namespace Builder.Factories
{
    public class PremuimEquipment : FactoryBase
    {
        public PremuimEquipment(CarBuilderBase builder) : base(builder)
        {
        }

        public override Car CarConstruct()
        {
            CarBuilder.BuildEngine();
            CarBuilder.BuildWheels();
            CarBuilder.BuildFrame();
            CarBuilder.BuildMultimedia();
            CarBuilder.BuildSafety();
            CarBuilder.BuildLuxury();

            return CarBuilder.GetCar();
        }
    }
}
