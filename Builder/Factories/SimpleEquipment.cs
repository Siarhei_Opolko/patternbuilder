﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Builder.Builders;

namespace Builder.Factories
{
    public class SimpleEquipment : FactoryBase
    {
        public SimpleEquipment(CarBuilderBase builder) : base(builder)
        {
        }

        public override Car CarConstruct()
        {
            CarBuilder.BuildFrame();
            CarBuilder.BuildEngine();
            CarBuilder.BuildWheels();

            return CarBuilder.GetCar();
        }
    }
}
