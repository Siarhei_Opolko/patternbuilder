﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Builder.Builders;

namespace Builder.Factories
{
    public abstract class FactoryBase
    {
        protected readonly CarBuilderBase CarBuilder;

        protected FactoryBase(CarBuilderBase builder)
        {
            CarBuilder = builder;
        }

        public abstract Car CarConstruct();
    }
}
