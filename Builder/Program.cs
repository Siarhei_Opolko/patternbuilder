﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Builder.Builders;
using Builder.Factories;

namespace Builder
{
    class Program
    {
        static void Main(string[] args)
        {
            FactoryBase factory = new PremuimEquipment(new SkodaBuilder());
            Console.WriteLine(factory.CarConstruct());

            factory = new SimpleEquipment(new SkodaBuilder());
            Console.WriteLine(factory.CarConstruct());

            factory = new PremuimEquipment(new AudiBuilder());
            Console.WriteLine(factory.CarConstruct());

            Console.WriteLine(factory.CarConstruct());

            Console.ReadLine();
        }
    }
}
