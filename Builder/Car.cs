﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Builder
{
    public class Car
    {
        public string Engine { get; set; }
        public string Frame { get; set; }
        public string Luxury { get; set; }
        public string Multimedia { get; set; }
        public string Safety { get; set; }
        public string Wheels { get; set; }

        public override string ToString()
        {
            var stringBuilder = new StringBuilder();
            stringBuilder.AppendFormat("Frame: {0}\n",Frame);
            stringBuilder.AppendFormat("Engine: {0}\n",Engine);
            stringBuilder.AppendFormat("Wheels: {0}\n",Wheels);
            stringBuilder.AppendFormat("Multimedia: {0}\n",Multimedia);
            stringBuilder.AppendFormat("Safety: {0}\n",Safety);
            stringBuilder.AppendFormat("Luxury: {0}\n",Luxury);
           
            return stringBuilder.ToString();
        }
    }
}
