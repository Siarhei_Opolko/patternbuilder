﻿namespace Builder.Builders
{
    class SkodaBuilder : CarBuilderBase
    {
        public SkodaBuilder() : base()
        { }
        public override void BuildMultimedia()
        {
            Car.Multimedia = "Skoda Multimedia";
        }

        public override void BuildWheels()
        {
            Car.Wheels += "R17 Skoda";
        }

        public override void BuildEngine()
        {
            Car.Engine = "1.8 TSI";
        }

        public override void BuildFrame()
        {
            Car.Frame = "Skoda Frame";
        }

        public override void BuildLuxury()
        {
            Car.Luxury = "Skoda Luxury";
        }

        public override void BuildSafety()
        {
            Car.Safety = "Skoda Safety";
        }
    }
}
