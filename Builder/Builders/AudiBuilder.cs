﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Builder.Builders
{
    class AudiBuilder : CarBuilderBase
    {
        public AudiBuilder()
            : base()
        { }
        public override void BuildMultimedia()
        {
            Car.Multimedia = "Audi Multimedia";
        }

        public override void BuildWheels()
        {
            Car.Wheels += "R18 Audi";
        }

        public override void BuildEngine()
        {
            Car.Engine = "2.0 TDI";
        }

        public override void BuildFrame()
        {
            Car.Frame = "Audi Frame";
        }

        public override void BuildLuxury()
        {
            Car.Luxury = "Audi Luxury";
        }

        public override void BuildSafety()
        {
            Car.Safety = "Audi Safety";
        }
    }
}
